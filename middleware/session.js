/*
 * @Description: session中间件
 * @Author: YangHeng
 * @Date: 2021-08-09 16:48:57
 * @FilePath: \web_server\node_modules\civiapp\middleware\session.js
 */
const session = require("koa-generic-session");
const FileStore = require("koa-generic-session-file2");

module.exports = (option = {}) => {
  civi.app.keys = ["keys", "keykeys"];
  return session({
    store: new FileStore({
      sessionDirectory: option.sessionDirectory
    })
  })
}