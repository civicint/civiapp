/*
 * @Description: 错误处理中间件
 * @Author: YangHeng
 * @Date: 2021-08-09 16:53:52
 * @FilePath: \web_server_app\middleware\error.js
 */

module.exports = () => {
  return async (ctx, next) => {
    await next().then(() => {
      if (ctx.res.statusCode === 404) {
        ctx.status = ctx.res.statusCode
        ctx.type = 'json'
        ctx.body = {code: ctx.res.statusCode, msg: `url \`${ctx.path}\` not found.`}
      }
    }).catch(error => {
      civi.logs.error(error)
      ctx.status = 500
      ctx.type = 'json'
      ctx.body = {code: 500, msg: error.message}
    })
  }
}