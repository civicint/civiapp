/*
 * @Description: 参数解析中间件
 * @Author: YangHeng
 * @Date: 2021-08-09 16:26:56
 * @FilePath: \web_server\node_modules\civiapp\middleware\parser.js
 */

// http://www.ptbird.cn/koa-body.html 参考文档
const koaBody = require('koa-body')

module.exports = () => {
  return koaBody({
    multipart: true, // 支持文件上传
  })
}