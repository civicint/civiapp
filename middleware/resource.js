/*
 * @Description: 静态资源中间件
 * @Author: YangHeng
 * @Date: 2021-08-09 16:26:56
 * @FilePath: \web_server\node_modules\civiapp\middleware\resource.js
 */

const path = require('path');
const static = require('koa-static'); // 引入静态资源中间件
const defaultOptions = {
  root: path.join(civi.ROOT_PATH, 'www'),
  publicPath: /^\/(static\/|favicon\.ico)/
}

module.exports = (options) => {
  options = Object.assign({}, defaultOptions, options);

  return async (ctx, next) => {
    if (options.publicPath.test(ctx.req.url)) {
      await static(options.root)(ctx, next)
    } else {
      await next()
    }
  }
}