/*
 * @Description: HTTP meta信息
 * @Author: YangHeng
 * @Date: 2021-08-09 15:25:51
 * @FilePath: \web_server_app\middleware\meta.js
 */


const { isFunction } = require('core-util-is')

/**
 * 默认配置
 */
const defaultOptions = {
  requestTimeout: 10 * 1000, // 请求超时, 默认10秒
  requestTimeoutCallback: () => {}, // 超时回调
  sendPowerBy: true, // 发送版权信息
  sendResponseTime: true, // send response time
  logRequest: true
};

/**
 * 发送 meta 中间价
 */
module.exports = (options) => {
  // 合并配置
  options = Object.assign({}, defaultOptions, options);

  // 返回KOA格式的中间件内容
  return (ctx, next) => {
    // 设置超时回调
    if (isFunction(options.requestTimeoutCallback)) {
      ctx.res.setTimeout(options.requestTimeout, () => {
        options.requestTimeoutCallback(ctx, options);
      });
    }

    // 发送版权信息
    if (options.sendPowerBy && !ctx.res.headersSent) {
      ctx.res.setHeader('X-Powered-By', `civicint`);
    }

    if (options.sendResponseTime || options.logRequest) {
      const startTime = Date.now();
      let err;
      return next().catch(e => {
        err = e;
      }).then(() => {
        const endTime = Date.now();
        if (options.sendResponseTime && !ctx.res.headersSent) {
          ctx.res.setHeader('X-Response-Time', `${endTime - startTime}ms`);
        }

        if (options.logRequest) {
          process.nextTick(() => {
            if (ctx.status === 500) {
              civi.logs.error(`${ctx.method} ${ctx.url} ${ctx.status} ${endTime - startTime}ms`)
            } else {
              if (ctx.isLogs !== false) {
                civi.logs.info(`${ctx.method} ${ctx.url} ${ctx.status} ${endTime - startTime}ms`)
              }
            }
          });
        }

        if (err) return Promise.reject(err);
      })
    } else {
      return next();
    }
  }
}