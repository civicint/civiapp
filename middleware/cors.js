/*
 * @Description: 跨域中间件
 * @Author: YangHeng
 * @Date: 2021-08-09 16:48:57
 * @FilePath: \web_server\node_modules\civiapp\middleware\cors.js
 */


module.exports = () => {
  return async (ctx, next) => {
    let cross = ctx.req.headers.referer ? ctx.req.headers.referer.substring(0, ctx.req.headers.referer.length - 1) : "*";

    ctx.res.setHeader("Access-Control-Allow-Origin", cross);
    ctx.res.setHeader("Access-Control-Allow-Credentials", "true")
    ctx.res.setHeader('Access-Control-Allow-Headers', 'Content-Type, X-Token, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    //设置默认类型为utf-8
    // ctx.res.setHeader("Content-Type", "text/html; charset=utf-8");
    await next()
  }
}