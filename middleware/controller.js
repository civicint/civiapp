/*
 * @Description: 路由解析中间件
 * @Author: YangHeng
 * @Date: 2021-08-09 16:53:52
 * @FilePath: \web_server_app\middleware\controller.js
 */

const url = require("url");
const path = require("path");
const fs = require("fs-extra");
const { isFunction } = require('core-util-is')

module.exports = () => {
  return async (ctx, next) => {
    // console.log('ctx.controller', ctx.controller)
    let path_arr = ctx.controller.split('/')
    let matchController = null
    civi.controllerList.some(item => {
      if (item.name.indexOf(ctx.controller) !== -1) {
        matchController = item
        return true
      }
    })

    if (matchController) {
      let controller = new matchController.controller(ctx, next)
      let result = null
      // 判断前置方法是否存在
      if (isFunction(controller.__before)) {
        result = await controller.__before()
      }

      // 执行控制器Action
      if (result !== false && isFunction(controller[matchController.action + 'Action'])) {
        result = await controller[matchController.action + 'Action']()
      }

      // 判断后方法是否存在
      if (result !== false && isFunction(controller.__after)) {
        result = await controller.__after()
      }

      if (!ctx.body) ctx.body = "OK"
      ctx.isLogs = controller.isLogs
      return
    } else {
      civi.logs.warn(`没有找到控制器: ${ctx.controller}`)
    }



    // // 匹配直接控制器文件
    // let controller_path = path.join(civi.APP_PATH, 'controller', ctx.controller + '.js')
    // let ext = await fs.exists(controller_path)
    // let actionName = 'index'
    // if (!ext) {
    //   // 匹配默认主文件
    //   controller_path = path.join(civi.APP_PATH, 'controller', ctx.controller + '/index.js')
    //   ext = await fs.exists(controller_path)
    //   if (!ext) {
    //     // 匹配控制器内的子方法
    //     actionName = path_arr.pop()
    //     controller_path = path.join(civi.APP_PATH, 'controller', path_arr.join('/') + '/index.js')
    //     ext = await fs.exists(controller_path)
    //   }
    // }

    // if (ext) {
    //   // 引入控制器实例 actionName
    //   let Controller = require(controller_path)
    //   if (Controller.isConstructor) {
    //     let controller = new Controller(ctx, next)
    //     let result = null
    //     // 判断前置方法是否存在
    //     if (isFunction(controller.__before)) {
    //       result = await controller.__before()
    //     }

    //     // 执行控制器Action
    //     if (result !== false && isFunction(controller[actionName + 'Action'])) {
    //       result = await controller[actionName + 'Action']()
    //     }

    //     // 判断后方法是否存在
    //     if (result !== false && isFunction(controller.__after)) {
    //       result = await controller.__after()
    //     }

    //     if (!ctx.body) ctx.body = "OK"
    //     return
    //   }
    // }
    
    // civi.logs.warn(`没有找到控制器: ${ctx.controller}`)
  }
}