/*
 * @Description: 中间件
 * @Author: YangHeng
 * @Date: 2021-08-09 14:03:11
 * @FilePath: \web_server\node_modules\civiapp\middleware\index.js
 */
const path = require('path')

module.exports = [
  // 写入HTTP元数据
  {
    handle: 'meta',
    options: {
      logRequest: civi.isDev,
      sendResponseTime: civi.isDev
    }
  },
  // 跨域中间件
  {
    handle: 'cors'
  },
  // session数据
  {
    handle: 'session',
    options: {
      sessionDirectory: path.join(civi.RUNTIME_PATH, 'sessions')
    }
  },
  // 静态资源处理
  {
    handle: 'resource'
  },
  // 错误处理
  {
    handle: 'error',
    options: {}
  },
  // HTTP请求参数解析
  {
    handle: 'parser',
  },
  // 路由解析
  {
    handle: 'router',
  },
  // 参数校验
  // {
  //   handle: 'validation',
  // },
  // 请求控制器
  {
    handle: 'controller',
  }
]