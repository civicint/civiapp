/*
 * @Description: 模拟上下文
 * @Author: YangHeng
 * @Date: 2021-08-10 10:05:07
 * @FilePath: \web_server_app\mock_context.js
 */

class MockContext {
  /**
   * @description: 失败返回参数
   * @param {*} data
   * @return {boolean}
   */
   error (msg, code) {
    throw Error(msg)
  }
}

module.exports = new MockContext()