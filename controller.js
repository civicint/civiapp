/*
 * @Description: 请求控制器类
 * @Author: YangHeng
 * @Date: 2021-08-10 10:05:07
 * @FilePath: \web_server_app\controller.js
 */
const path = require('path');
const fs = require('fs-extra')
const send = require('koa-send');
const template = require('art-template');
const { isNullOrUndefined, isObject } = require('core-util-is')

module.exports = class Controller {
  constructor(ctx, next) {
    this.ctx = ctx
    this.next = next
  }
  /**
   * @description: 是否是控制器用于验证控制器有效
   * @return {boolean}
   */
  static get isConstructor() {
    return true
  }
  /**
   * 重定向跳转
   * @param {*} url 
   */
  redirect(url, status = 301) {
    this.ctx.status = status
    this.ctx.redirect(url)
  }
  /**
   * @description: header参数获取 设置
   * @param {*} name 参数名称
   * @param {*} value 参数值
   */
  header(name, value) {
    let headParams = this.ctx.headers
    // 没有参数返回全部的header参数
    if (isNullOrUndefined(name)) {
      return headParams
    } else {
      // value 存在设置参数
      if (!isNullOrUndefined(value)) {
        headParams[name] = value
      }
      // 返回参数内容
      return headParams[name]
    }
  }
  /**
   * @description: 获取webSocker 数据
   */
  get wsData() {
    return this.ctx.req.wsData
  }
  /**
   * @description: 获取ws连接实例
   */
  get ws() {
    return this.ctx.req.ws
  }
  /**
   * @description: cookie
   */
  get cookie() {
    return this.ctx.cookies
  }
  /**
   * @description: session
   */
  get session() {
    return this.ctx.session
  }
  /**
   * @description: cache
   */
  get cache() {
    return civi.cache
  }
  /**
   * @description: method
   */
  get method() {
    return this.ctx.method
  }
  async download(_path, option) {
    await send(this.ctx, _path, option)
  }
  /**
   * @description: 成功返回参数
   * @param {*} data
   * @return {boolean}
   */
  success(data) {
    this.ctx.type = 'json'
    this.ctx.body = { code: 0, msg: 'success', data }
    return false
  }
  /**
   * @description: 失败返回参数
   * @param {*} data
   * @return {boolean}
   */
  error(msg, code) {
    this.ctx.type = 'json'
    this.ctx.body = { code: code || 1001, msg }
    return false
  }
  /**
   * @description: GET参数获取 设置
   * @param {*} name 参数名称
   * @param {*} value 参数值
   */
  get(name, value) {
    let getParams = this.ctx.request.query
    // 没有参数返回全部的GET参数
    if (isNullOrUndefined(name)) {
      return JSON.parse(JSON.stringify(getParams))
    } else {
      // value 存在设置参数
      if (!isNullOrUndefined(value)) {
        getParams[name] = value
      }
      // 返回参数内容
      return getParams[name]
    }
  }
  /**
   * @description: POST参数获取 设置
   * @param {*} name 参数名称
   * @param {*} value 参数值
   */
  post(name, value) {
    let postParams = this.ctx.request.body
    // 没有参数返回全部的GET参数
    if (isNullOrUndefined(name)) {
      return postParams
    } else {
      // value 存在设置参数
      if (!isNullOrUndefined(value)) {
        postParams[name] = value
      }
      // 返回参数内容
      return postParams[name]
    }
  }
  /**
   * @description: 获取文件参数
   * @param {*} name 文件名
   * @return {*}
   */
  file(name) {
    let filesParams = this.ctx.request.files
    // 没有参数返回全部的GET参数
    if (isNullOrUndefined(name)) {
      return filesParams
    } else {
      return filesParams[name]
    }
  }
  /**
   * @description: 显示页面
   * @param {*} filename html模板名称 位于项目根目录下的view文件夹
   * @param {*} params 模板参数
   */
  display(filename, params = {}) {
    if (isObject(filename)) {
      params = filename
      filename = ''
    }

    filename = filename || this.ctx.controller || 'index'
    let viewPath = path.join(civi.ROOT_PATH, 'view', filename + '.html')
    if (fs.existsSync(viewPath)) {
      this.ctx.type = 'html'
      this.ctx.body = template(viewPath, params)
      return false
    } else {
      return this.error('The template file `view/' + filename + '.html' + '` does not exist', 500)
    }
  }
}