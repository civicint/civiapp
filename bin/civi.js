#!/usr/bin/env node

const root_path = process.env.INIT_CWD
const type = process.argv[2]

if (type === 'controller') {
  logSuccess('创建控制器')
} else if (type === 'model') {
  logSuccess('创建数据库模型')
} else {
  logError('Command error, please check the document')
}


function logError (msg) {
  console.log('\033[41;33m ERROR \033[40;31m', msg, '\033[0m')
}

function logSuccess (msg) {
  console.log('\033[42;37mSUCCESS\033[40;32m', msg, '\033[0m')
}