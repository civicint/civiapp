import * as Mongoose from 'mongoose';
import * as Koa from 'koa';
import * as http from 'http';
import * as coreUtilIs from 'core-util-is';

/**@description: 日志类 */
interface CiviLogs {
  success(...data): void,
  error(...data): void
  warn(...data): void
  debug(...data): void
  info(...data): void
}

/**@description: 项目配置 */
interface CiviConfig {
  /**@description: HTTP服务配置 */
  service: CiviServiceConfig,
  /**@description: 缓存配置 */
  cache: CiviCacheConfig,
  /**@description: 日志配置 */
  log4js: CiviLogsConfig,
  /**@description: Mongo数据库配置 */
  mongo: CiviMongoConfig,
  wss: CiviWssConfig
}

interface CiviServiceConfig { port: number, host: string }

/**@description: 缓存配置 */
interface CiviCacheConfig {
  dir: string
}

enum CiviLogsConfigType { 'console', 'dateFile' };
/**@description: 缓存配置 */
interface CiviLogsConfig {
  /** @description: 日志输出类型*/
  type: CiviLogsConfigType,
  /** @description: 日志存储路径*/
  dirname: string,
  /**
   * @description: 日志文件命名格式
   * @param {*} yyyy-MM-dd.log
   */
  pattern: string
}

interface CiviMongoConfig {
  host: string | string[],
  port: number | number[],
  /** @description: 公共字段*/
  commonField: Mongoose.SchemaDefinition<MongooseDocumentDefinition<Mongoose.SchemaDefinitionType>>,
  /** @description: 数据预处理方法*/
  pre: CiviMongoPreFun
}
interface CiviMongoPreFun {
  [propName: string]: function (next): void;
}

interface CiviWssConfig {
  path: string,
  messages: { [propName: string]: string }
}

interface MyAddon {
  ROOT_PATHaa: string;
}
declare module civi {
  function isArray(arg: any): boolean;
  function isBoolean(arg: any): boolean;
  function isBuffer(b: any): boolean;
  function isDate(d: any): boolean;
  function isError(e: any): boolean;
  function isFunction(arg: any): boolean;
  function isNull(arg: any): boolean;
  function isNullOrUndefined(arg: any): boolean;
  function isNumber(arg: any): boolean;
  function isObject(obj: any): boolean;
  function isPrimitive(arg: any): boolean;
  function isRegExp(re: any): boolean;
  function isString(arg: any): boolean;
  function isSymbol(arg: any): boolean;
  function isUndefined(arg: any): boolean;

  /**@description: 项目根目录 */
  let ROOT_PATH: string;
  /**@description: 项目src目录 */
  let APP_PATH: string;
  /**@description: 运行时目录 */
  let RUNTIME_PATH: string;
  /**@description: 项目运行环境 */
  let env: string;
  /**@description: 是否开发环境 */
  let isDev: boolean;
  /**@description: koa实例 */
  let app: Koa;
  /**@description: 项目配置 */
  let config: CiviConfig;
  /**@description: 日志打印 */
  let logs: CiviLogs;
  let httpServer: http.Server;
  /**@description: UUID */
  function uuid(type: string): string;
  /**@description: 控制器 */
  class Controller {
    constructor(ctx: Koa.Context, next: function(): Promise<void>): Controller;
    public ctx: Koa.Context;
    public next: function(): Promise<void>;
    public isConstructor: boolean;
    public header(name: string, value: any): any;
    public redirect(url: string): any;
    public wsData: any;
    public ws: any;
    public cookie: any;
    public session: any;
    public cache: any;
    public method: string;
    public download(_path: string): void;
    public success(data: any): boolean;
    public error(msg: string, code: number): boolean;
    public get(name: string, value: any): any;
    public post(name: string, value: any): any;
    public file(name: string): any;
    public display(filename: string, params: Object): any;
    /**重定向 */
    public redirect(url: string, status: number): any;
  };
  let command: {
    /**@description: 执行指令 */
    execSync(command: string): Promise<string>
  };
  /**@description:系统忙碌状态 */
  let busyState: boolean;

  let aescode: {
    /**加密 */
    encrypt(str: string): string;
    /**解密 */
    decrypt(hex: string): string;
  }
  /**
   * 阿里OSS工具
   */
  let oss: {
    /**
     * 通过文件路径上传文件
     * @param {string} filePath 
     * @param {string} ossname 
     * @returns ossname
     */
    upload(filePath: string, ossname?: string): Promise<string>;
  }
  let lic: {
    /**解析license 文件内容 */
    decrypt(hex: string): string;
  }

  let up: {
    /**升级状态 */
    status: boolean;
    /**执行升级操作 */
    execUpdate(): Promise<void>
  }

  /**@description: ffmpeg工具 */
  let ffmpeg: {
    /**@description: 获取rtsp的预览图片 */
    getRtspImage(rtsp: string): Promise<string>;
    /**@description: 多张图片合成视频 */
    imgsToVideo(source: string, fps: number, output: string, start: number, time: number, size: string): Promise<any>;
  };
  /**@description: 模拟上下文 */
  let mock_context: Controller;
  /**@description: 基础控制器 */
  class BaseController extends Controller {
    public plg: Plugin;
  }
  /**@description: 插件实例 */
  let plugins: {
    [propName: string]: Plugin
  };
  /**@description: 缓存操作 */
  let cache: {
    get(name: string): any;
    set(name: string, value: any): void;
  };
  /**@description: 数据库类 */
  let DB: MongooseModel;
  /**@description: 数据库操作 */
  function db(name: string): MongooseModel<T>;
  /**@description: 路由表 */
  let routerTable: [string[]]
  /**@description: 控制器操作 */
  function action(
    controllerName: string,
    query: Object, body: Object, files: Object,
    params: Object
  ): any;
  /**@description: 插件类 */
  class Plugin {
    constructor(info: object): Plugin;
    public info: object;
    public injectRouter(routers: Array<Array<string>>): void;
    public injectModel(): void;
    public db(name: string): MongooseModel<T>;
    /**设置算法忙碌状态 */
    public setBusyState(status: boolean): void;
    /**启动算法容器 */
    public startAlgApp(): Promise<void>;
    /**停止算法容器 */
    public stopAlgApp(): Promise<void>;
    /**卸载插件 */
    public uninstall(): Promise<void>;
    public id: string;
    public name: string;
    public version: string;
    public description: string;
    public author: string;
    public use: number;
    public defaultConfig: object;
    public path: string;
  };
}

// Object.assign(civi, coreUtilIs)


/**@description: 数据库类 */
interface MongooseModel<T extends Mongoose.Document = Mongoose.Document> extends Mongoose.Model<T> {
  new(modelName?: string): MongooseModel<T>;
  readonly tablePrefix: string;
  readonly tableName: string;
  static isModel: boolean;
  static getModels(name: string): DB;
  db<U extends MongooseModel<T>>(name: string): U;
  /**@description: ObjectId获取转换 */
  objId(id: null | string | Mongoose.Types.ObjectId): Mongoose.Types.ObjectId;
  /**@description: 通过ID查找文档 */
  getId(id: string | Mongoose.Types.ObjectId): Mongoose.Document;
  /**@description: 添加单条数据 */
  add(data: Object): Mongoose.Document;
  /**@description: 添加多条数据 */
  addList(data: Object[]): Mongoose.Document[];
  /**@description: 删除数据 */
  del(id: string | Mongoose.Types.ObjectId): void;
  /**@description:  编辑数据 */
  edit(data: Object): Mongoose.Document;
  /**@description: 获取列表 */
  getList(
    json: Mongoose.FilterQuery,
    page: number, size: number,
    sort: string | any,
    filter: any | null
  ): Mongoose.Document;
}

export = civi
export as namespace civi