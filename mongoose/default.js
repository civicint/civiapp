/*
 * @Description: mongo 数据库默认配置
 * @Author: YangHeng
 * @Date: 2021-08-12 10:15:50
 * @FilePath: \web_server_app\mongoose\default.js
 */

module.exports = {
  host: '127.0.0.1', // 数据库主机
  port: 27017, // 数据库端口
  // 数据模型公共字段
  commonField: {
    create_at: {type: Number, default: Date.now}, // 记录创建时间
    update_at: {type: Number, default: 0}, // 记录更新时间
  },
  // 公用的前置 HOOK 参考文档 http://www.mongoosejs.net/docs/api.html#schema_Schema-pre
  pre: {
    // 保存前 
    'save': function (next) {
      this.create_at = this.create_at || Date.now();
      next()
    },
    // 更新前
    'update': function(next) {
      this._update.update_at = Date.now();
      next();
    },
    // 更新前
    'updateOne': function(next) {
      this._update.update_at = Date.now();
      next();
    },
    // 更新前 操作
    'findOneAndUpdate': function(next) {
      this._update.update_at = Date.now();
      next();
    },
    // 更新前 操作
    'updateMany': function(next) {
      this._update.update_at = Date.now();
      next();
    },
  }
}