/*
 * @Description: 用于在程序内操作控制器
 * @Author: YangHeng
 * @Date: 2021-08-12 15:37:40
 * @FilePath: \web_server_app\action\index.js
 */
const path = require("path");
const fs = require("fs-extra");
const { isFunction, isNullOrUndefined } = require('core-util-is')

class Action {
  constructor () {}
  /**
   * @description: 执行控制器
   * @param {String} controllerName 控制器名称
   * @param {Object} query 挂载GET参数
   * @param {Object} body 挂载POST参数
   * @param {Object} files 挂载FILES参数
   * @param {Object} params 挂载CTX上面的参数
   * @return {Promise} result
   */
  async run (controllerName, {query = {}, body = {}, files = {}, params = {}} = {}) {
    let matchController = null
    civi.controllerList.some(item => {
      if (item.name.indexOf(controllerName) !== -1) {
        matchController = item
        return true
      }
    })

    if (matchController) {
      let ctx = {
        controller: matchController.action,
        method: "ACTION", req: {}, res: {},
        request: { query, body, files }, 
        ...params
      }
      let controller = new matchController.controller(ctx, ()=>{})
      let result = null
      // 判断前置方法是否存在
      if (isFunction(controller.__before)) {
        result = await controller.__before()
      }

      // 执行控制器Action
      if (result !== false && isFunction(controller[matchController.action + 'Action'])) {
        result = await controller[matchController.action + 'Action']()
      }

      // 判断后方法是否存在
      if (result !== false && isFunction(controller.__after)) {
        result = await controller.__after()
      }
      
      if (!ctx.body) ctx.body = "OK"
      if (!isNullOrUndefined(result)) return result
      return ctx.body
    }
    return null



    // // 分割控制器
    // let path_arr = controllerName.split('/')

    // // 匹配直接控制器文件
    // let controller_path = path.join(civi.APP_PATH, 'controller', controllerName + '.js')
    // let ext = await fs.exists(controller_path)
    // let actionName = 'index'
    // if (!ext) {
    //   // 匹配默认主文件
    //   controller_path = path.join(civi.APP_PATH, 'controller', controllerName + '/index.js')
    //   ext = await fs.exists(controller_path)
    //   if (!ext) {
    //     // 匹配控制器内的子方法
    //     actionName = path_arr.pop()
    //     controller_path = path.join(civi.APP_PATH, 'controller', path_arr.join('/') + '/index.js')
    //     ext = await fs.exists(controller_path)
    //   }
    // }

    // if (ext) {
    //   // 引入控制器实例 actionName
    //   let Controller = require(controller_path)
    //   if (Controller.isConstructor) {
    //     let ctx = {
    //       controller: actionName,
    //       method: "ACTION", req: {}, res: {},
    //       request: { query, body, files }, 
    //       ...params
    //     }
    //     let controller = new Controller(ctx, ()=>{})
    //     let result = null
    //     // 判断前置方法是否存在
    //     if (isFunction(controller.__before)) {
    //       result = await controller.__before()
    //     }

    //     // 执行控制器Action
    //     if (result !== false && isFunction(controller[actionName + 'Action'])) {
    //       result = await controller[actionName + 'Action']()
    //     }

    //     // 判断后方法是否存在
    //     if (result !== false && isFunction(controller.__after)) {
    //       result = await controller.__after()
    //     }

    //     if (!ctx.body) ctx.body = "OK"
        
    //     if (result) return result
    //     else return ctx.body
    //   }
    // }

    // return null
  }
}

module.exports = new Action()