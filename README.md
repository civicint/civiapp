# CiviApp
> 是一个基于KOA的后端服务框架，接口和文件目录上参考了thinkjs

## run 运行
```javascript
const path = require('path')
const Application = require('civiapp')

// 创建实例
const instance = new Application({
  ROOT_PATH: process.cwd(),
  APP_PATH: path.join(process.cwd(), 'src'),
  RUNTIME_PATH: path.join(process.cwd(), 'runtime'),
  env: 'development',
  // env: 'production' // 生产环境
})

// 启动服务
instance.start()
```

## Controller 控制器
```javascript
module.exports = class extends civi.Controller {
  async indexAction () {
    return this.display()
  }
}
```

