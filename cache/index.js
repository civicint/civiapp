/*
 * @Description: 缓存
 * @Author: YangHeng
 * @Date: 2021-08-10 17:18:35
 * @FilePath: \web_server_app\cache\index.js
 */
const path = require('path')
const fs = require('fs-extra')

const cachePath = path.join(civi.config.cache.dir, 'cache.json')
let ext = fs.existsSync(cachePath)
if (!ext) fs.emptyDirSync(civi.config.cache.dir)

module.exports = {
  /**
   * @description: 获取缓存
   * @param {string} name
   * @return {*}
   */
  get (name) {
    let ext = fs.existsSync(cachePath)
    if (!ext) return;
    try {
      let cache = fs.readJSONSync(cachePath)
      return cache[name]
    } catch (err) {
      return null
    }
  },
  /**
   * @description: 设置缓存
   * @param {string} name
   * @param {*} value
   */
  set (name, value) {
    let cache = {}
    let ext = fs.existsSync(cachePath)
    
    try {
      if (ext) cache = fs.readJSONSync(cachePath);
    } catch (err) {
      cache = {}
    }

    cache[name] = value
    fs.writeJSONSync(cachePath, cache)
  }
}